import random

print("Adding up 10 random numbers.")

y = 0
x = 0
for i in range(0, 10):
    x = random.randrange(1, 20)
    y = y + x
    
print("Total is {0:d}, last value was {1:d}".format(y, x))