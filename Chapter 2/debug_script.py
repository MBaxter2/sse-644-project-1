def doubleAdd(a, b, c):
    n = a + b
    m = b + c
    return n + m

def doubleDiv(a, b, c):
    n = a / b
    m = b / c
    return n / m

def doDouble(a, b, c):
    n = doubleAdd(a, b, c)
    m = doubleDiv(a, b, c)
    return n * m

d = doDouble(1, 2, 3)