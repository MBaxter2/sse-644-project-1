import random
import numpy as np

values_count = 10000000

values_slow = [random.random() for _ in range(values_count)]
values_fast = np.array(values_slow)
#values_fast = np.random.rand(values_count)

def error_slow(target, values):
    err = 0
    for i, x in enumerate(values):
        err += (target - x) ** 2
    return err / i

def error_fast(target, values):
    errs = np.square(values - target)
    return np.sum(errs) / values.shape[0]