import sys
import numpy as np
from matplotlib.figure import Figure
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from PyQt5 import QtCore
from PyQt5.QtWidgets import QApplication, QMainWindow, QWidget, QPushButton, QGridLayout, QLabel

class Grapher(QMainWindow):
    def __init__(self):
        QMainWindow.__init__(self)
        
        self.resize(300, 300)
        self.setWindowTitle("Grapher")
        
        centralWidget = QWidget(self)
        self.setCentralWidget(centralWidget)
        
        self.gridLayout = QGridLayout(self)
        centralWidget.setLayout(self.gridLayout)
        
        self.button = QPushButton("Toggle Graph", self)
        self.button.clicked.connect(self.clicked)
        
        self.label = QLabel("Push the button to show graph", self)
        self.label.setAlignment(QtCore.Qt.AlignCenter)        
        
        self.fig = Figure(figsize=(150, 150), dpi=100)
        self.axes = self.fig.add_subplot(111)
        self.axes.hold(False)
        
        self.fc = FigureCanvas(self.fig)   
        
        self.gridLayout.addWidget(self.label, 0, 0)
        self.gridLayout.addWidget(self.button, 1, 0)  
        self.gridLayout.addWidget(self.fc, 2, 0)  
        
        self.toggle = False
        
    def clicked(self):
        if(self.toggle == False):   
            x = np.linspace(0, 10, 50)
            y = x * 2 + 3
            self.axes.plot(x, y)
            self.fc.draw()
            self.toggle = True
        else:
            self.axes.plot([], [])
            self.fc.draw()
            self.toggle = False
        
        
if __name__ == '__main__':
    app = QtCore.QCoreApplication.instance()
    if app is None:
        app = QApplication(sys.argv)
    
    window = Grapher()
    window.show()
    
    sys.exit(app.exec_())