from ipyparallel import Client

rc = Client()

def slow_add(x, y):
    time.sleep(2)
    return x + y

v = rc[:]
with v.sync_imports():
    import time

asr = v.map(slow_add, range(10))

asr.ready(), asr.elapsed

asr.get()

asr.elapsed, asr.serial_time